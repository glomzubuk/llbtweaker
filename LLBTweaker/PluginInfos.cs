﻿using System.Reflection;
using LLBT;

#region Assembly attributes
[assembly: AssemblyVersion(PluginInfos.PLUGIN_VERSION)]
[assembly: AssemblyTitle(PluginInfos.PLUGIN_NAME + " (" + PluginInfos.PLUGIN_ID + ")")]
[assembly: AssemblyProduct(PluginInfos.PLUGIN_NAME)]
#endregion

namespace LLBT
{
    public static class PluginInfos
    {
        public const string PLUGIN_NAME = "LLBTweaker";
        public const string PLUGIN_ID = "fr.glomzubuk.plugins.llb.llbtweaker";
        public const string PLUGIN_VERSION = "0.2.2";
    }
}
