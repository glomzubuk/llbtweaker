﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using BepInEx;
using LLBML.States;
using BepInEx.Logging;
using LLBML.GameEvents;
using LLBML.Messages;
using LLBML.Networking;
using LLBML.Players;
using LLBML.Utils;
using LLBT.Tweaks;
using Multiplayer;

namespace LLBT
{
    enum LLBT_MSGCODES : ushort
    {
        CHECKIN = 380,
        CHECKIN_ACK,
        UNKNOWN_TWEAKS,
        ABORT,
    }

    internal static class Network
    {
        private static readonly ManualLogSource Logger = LLBTweaker.Log;
        private static bool[] playerStatuses = new bool[4];
        private static Dictionary<string, TweakConfigJson> lobbyTweakConfigs = new Dictionary<string, TweakConfigJson>();
        private static bool InVanillaLobby = true;

        internal static void Init()
        {
            PluginInfo llbtInfo = LLBTweaker.Instance.Info;
            PlayerLobbyState.RegisterPayload(llbtInfo, OnSendLobbyState, OnReceiveLobbyState);
            MessageApi.RegisterCustomMessage(llbtInfo, (ushort)LLBT_MSGCODES.CHECKIN,
                "LLBTCheckIn", ReceiveCheckIn);
            MessageApi.RegisterCustomMessage(llbtInfo, (ushort)LLBT_MSGCODES.CHECKIN_ACK,
                "LLBTCheckInAck", ReceiveCheckInAck);
            MessageApi.RegisterCustomMessage(llbtInfo, (ushort)LLBT_MSGCODES.UNKNOWN_TWEAKS,
                "LLBTUnknownTweaks", ReceiveUnknownTweaks);
            MessageApi.RegisterCustomMessage(llbtInfo, (ushort)LLBT_MSGCODES.ABORT,
                "LLBTAbort", ReceiveAbort);

            LobbyEvents.OnLobbyEntered += OnLobbyEnteredHandler;
            LobbyEvents.OnLobbyReady += OnLobbyReadyHandler;
            LobbyEvents.OnStageSelectOpen += OnStageSelectOpenedHandler;
            LobbyEvents.OnDecisions += OnDecisionsHandler;
            MenuEvents.OnMainMenuEntered += OnMainMenuEnteredHandler;
        }

        internal static void OnUpdate()
        {
            LLBTweaker.Instance.showVanillaWarning = AllPeersTweaked() == false;
        }

        internal static bool AllPeersTweaked()
        {
            if (P2P.localPeer == null) return true;

            bool allTweaked = true;
            P2P.localPeer.ForAllOthers(peer => {
                var status = Player.GetPlayer(peer.playerNr).playerStatus;
                if (status != PlayerStatus.NONE && status != PlayerStatus.DISCONNECTED)
                {
                    if (playerStatuses[peer.playerNr] == false)
                    {
                        allTweaked = false;
                    }
                }
            });
            return allTweaked;
        }

        internal static byte[] OnSendLobbyState(PlayerLobbyState pls)
        {
            byte[] result = null;
            if (Player.GetPlayer(pls.playerNr).peer is LocalHost host)
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (BinaryWriter binaryWriter = new BinaryWriter(memoryStream))
                    {
                        var json = TinyJson.JSONWriter.ToJson(lobbyTweakConfigs);
                        Logger.LogDebug($"Sending Lobby State: {json}");
                        binaryWriter.Write(json);
                    }
                    result = memoryStream.ToArray();
                }

            }
            return result;
        }

        internal static void OnReceiveLobbyState(PlayerLobbyState pls, byte[] payload)
        {
            var unknownTweaks = new List<string>();
            foreach (var index in lobbyTweakConfigs.Keys)
            {
                lobbyTweakConfigs[index].enabled = false;
            }

            using (MemoryStream memoryStream = new MemoryStream(payload))
            {
                using (BinaryReader binaryReader = new BinaryReader(memoryStream))
                {
                    var json = binaryReader.ReadString();
                    Logger.LogDebug($"Receiving LobbyState: {json}");
                    Dictionary<string, TweakConfigJson> jTweakConfigs = TinyJson.JSONParser.FromJson<Dictionary<string, TweakConfigJson>>(json);
                    foreach (var jTweakConfig in jTweakConfigs)
                    {
                        Logger.LogDebug($"{jTweakConfig.Key}:\n{jTweakConfig.Value}");
                        if (LLBTweaker.Instance.tweaks.Find(_tweak => _tweak.ID == jTweakConfig.Key) is TweakBase tweak)
                        {
                            lobbyTweakConfigs[jTweakConfig.Key] = jTweakConfig.Value;
                        }
                        else
                        {
                            unknownTweaks.Add(jTweakConfig.Key);
                        }
                    }
                }
            }

            if (unknownTweaks.Count > 0)
            {
                SendUnkownTweaksMessage(unknownTweaks);
            }
        }

        private static void OnMainMenuEnteredHandler(object source, OnMainMenuEnteredArgs e)
        {
            Logger.LogDebug("Entering MainMenu; Clearing all modified tweak configs");
            LLBTweaker.Instance.ReloadAllTweaksConfigs();
            lobbyTweakConfigs.Clear();
        }
        internal static void OnLobbyEnteredHandler(object source, LobbyEventArgs args)
        {
            if (LLBTweaker.Instance.restoreConfigsOnEnteringLobby.Value)
            {
                LLBTweaker.Instance.ReloadAllTweaksConfigs();
            }

            lobbyTweakConfigs.Clear();
            foreach (var tweak in LLBTweaker.Instance.tweaks)
            {
                lobbyTweakConfigs[tweak.ID] = tweak.ConfigToJson();
            }

            playerStatuses.Fill(false);
            InVanillaLobby = true;

        }
        internal static void OnLobbyReadyHandler(object source, LobbyReadyArgs args)
        {
            if (args.isOnline)
            {
                SendCheckIn();
            }
        }

        internal static void OnStageSelectOpenedHandler(HDLIJDBFGKN source, OnStageSelectOpenArgs args)
        {
            if (!NetworkApi.IsOnline) return;
            if (InVanillaLobby)
            {
                Logger.LogDebug("Entering Stage Select and current lobby is vanilla.");
                LLBTweaker.Instance.DisableAll();
                return;
            }

            if (Player.GetLocalPlayer().peer is LocalHost host)
            {
                if (!Network.AllPeersTweaked())
                {
                    P2P.SendAll(new Message((Msg)LLBT_MSGCODES.ABORT, 0));
                }
            }
        }

        internal static void OnDecisionsHandler(HDLIJDBFGKN source, OnDecisionsArgs args)
        {
            if (!InVanillaLobby && !args.sent)
            {
                LLBTweaker.Instance.LoadTweakConfigs(lobbyTweakConfigs);
                LLBTweaker.Instance.ReloadAllTweaksStates();
            }
        }

        internal static void SendCheckIn()
        {
            var localPlayerNr = P2P.localPeer.playerNr;

            Logger.LogDebug($"[P{localPlayerNr}] Sending CheckIn to P0.");
            P2P.SendToPlayerNr(0, new Message(
                (Msg)LLBT_MSGCODES.CHECKIN,
                localPlayerNr, 0
            ));
        }
        internal static void ReceiveCheckIn(Message payload)
        {
            var localPlayerNr = P2P.localPeer.playerNr;
            var remotePlayerNr = payload.playerNr;
            Logger.LogDebug($"[P{localPlayerNr}] Received CheckIn from P{remotePlayerNr}.");

            playerStatuses[remotePlayerNr] = true;

            Logger.LogDebug($"[P{localPlayerNr}] Sending CheckInAck to P{remotePlayerNr}.");
            P2P.SendToPlayerNr(remotePlayerNr, new Message(
                (Msg)LLBT_MSGCODES.CHECKIN_ACK,
                localPlayerNr, remotePlayerNr
            ));
        }

        internal static void ReceiveCheckInAck(Message payload)
        {
            var localPlayerNr = P2P.localPeer.playerNr;
            var remotePlayerNr = payload.playerNr;
            Logger.LogDebug($"[P{localPlayerNr}] Received CheckInAck from P{remotePlayerNr}.");

            InVanillaLobby = false;
        }

        internal static void SendUnkownTweaksMessage(List<string> unknownTweaks)
        {
            var localPlayerNr = P2P.localPeer.playerNr;
            var jUnknownTweaks = TinyJson.JSONWriter.ToJson(unknownTweaks);

            Logger.LogDebug($"[P{localPlayerNr}] Sending UnkownTweaks to P0 with content:\n{jUnknownTweaks}");

            byte[] utBytes = Encoding.ASCII.GetBytes(jUnknownTweaks);
            P2P.SendToPlayerNr(0, new Message(
                (Msg)LLBT_MSGCODES.UNKNOWN_TWEAKS,
                P2P.localPeer.playerNr, 0,
                utBytes, utBytes.Length
            ));
        }

        internal static void ReceiveUnknownTweaks(Message payload)
        {
            if (payload.ob == null) {
                LLBTweaker.Log.LogWarning("Received a null payload on an UnkownTweaks message. Ignoring." );
                return;
            }

            var localPlayerNr = P2P.localPeer.playerNr;
            var remotePlayerNr = payload.playerNr;

            var jMissingTweaks = Encoding.ASCII.GetString(payload.ob as byte[]);

            Logger.LogDebug($"[P{localPlayerNr}] Received UnknownTweaks from P{remotePlayerNr} with content:\n{jMissingTweaks}");

            var missingTweaks = TinyJson.JSONParser.FromJson<List<string>>(jMissingTweaks);

            GameStatesLobbyUtils.MakeSureReadyIs(false, true);

            TmpRemoveMissingTweaks(missingTweaks);
            GameStatesLobbyUtils.SendPlayerState(Player.GetLocalPlayer());
        }

        internal static void TmpRemoveMissingTweaks(List<string> missingTweakIDs)
        {
            foreach (TweakBase tweak in LLBTweaker.Instance.tweaks)
            {
                if (missingTweakIDs.Contains(tweak.ID))
                {
                    Logger.LogDebug($"Unpatching {tweak.Name} on request." );
                    lobbyTweakConfigs.Remove(tweak.ID);
                }
            }
        }

        internal static void ReceiveAbort(Message payload)
        {
            var localPlayerNr = P2P.localPeer.playerNr;
            var remotePlayerNr = payload.playerNr;
            Logger.LogDebug($"[P{localPlayerNr}] Received Abort from P{remotePlayerNr}.");
            LLBTweaker.Instance.DisableAll();
        }
    }
}
