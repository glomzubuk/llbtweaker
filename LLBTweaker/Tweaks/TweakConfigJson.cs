using System.Collections.Generic;
using HarmonyLib;

namespace LLBT.Tweaks
{
    public class TweakConfigJson
    {
        public bool enabled;
        public List<ConfigEntryJson> configs;

        public override string ToString()
        {
            return $"enabled: {enabled}\n" +
                   $"Configs:\n\t{configs.Join(ce => ce.ToString(), "\n\t")}";
        }

    }
    public struct ConfigEntryJson
    {
        public string Section;
        public string Key;
        public string value;

        public override string ToString() => $"[{Section}.{Key}]: {value}";
    }
}
