﻿using System;
using System.Reflection;
using HarmonyLib;

namespace LLBT.Tweaks { 
    public class FieldTweak<TClass, TField> : TweakBase where TClass:class
    {
        protected readonly AccessTools.FieldRef<TClass, TField> fieldRef;
        protected readonly TField newValue;
        protected TField initialValue;
        protected TClass instance;

        public static FieldTweak<_TClass, _TField> Create<_TClass, _TField>(FieldInfo _fieldInfo, _TField _newValue) where _TClass : class
        {
            string _patchID = "FieldPatch_" + _fieldInfo.Name;
            string _patchName = _fieldInfo.Name;
            return new FieldTweak<_TClass, _TField>(
                _patchID,
                _patchName,
                _fieldInfo,
                _newValue
            );
        }
        public static FieldTweak<_TClass, _TField> Create<_TClass, _TField>(FieldInfo _fieldInfo, _TField _newValue, _TClass _instance) where _TClass : class
        {
            string _patchID = "FieldPatch_" + _fieldInfo.Name;
            string _patchName = _fieldInfo.Name;
            return new FieldTweak<_TClass, _TField>(
                _patchID,
                _patchName,
                _fieldInfo,
                _newValue,
                _instance
            );
        }
        public FieldTweak(string _patchID, string _patchName, FieldInfo _fieldInfo, TField _newValue, TClass _instance = null)
            : base(_patchID, _patchName)
        {
            this.fieldRef = AccessTools.FieldRefAccess<TClass, TField>(_fieldInfo);
            this.newValue = _newValue;
            this.instance = _instance;
        }

        public void SetInstance(TClass _instance)
        {
            this.instance = _instance;
        }

        public TField GetFieldValue()
        {
            return this.fieldRef.Invoke(instance);
        }

        #region overrides
        protected override void DoPatch()
        {
            if (!IsEnabled && instance != null)
            {
                try
                {
                    TField field = this.fieldRef.Invoke(instance);
                    this.initialValue = field;
                    field = newValue;
                } catch ( Exception e )
                {

                }
            }
            throw new NotImplementedException();
        }

        protected override void DoUnpatch()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
