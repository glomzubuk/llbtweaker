﻿using System.Collections.Generic;
using BepInEx.Configuration;
using BepInEx.Logging;
using HarmonyLib;

namespace LLBT.Tweaks
{
    /// <summary>
    /// A class to allow LLBTweaker to handle any form of patches.
    /// </summary>
    public abstract class TweakBase
    {
        /// <summary>
        /// Access to LLBTweaker's logger.
        /// </summary>
        public static ManualLogSource Logger => LLBTweaker.TweakLog;
        protected static ConfigFile Config => LLBTweaker.Instance.TweakConfig;

        /// <summary>
        /// Persistent storage for whether or not the patch should be enabled on start.
        /// </summary>
        protected ConfigEntry<bool> tweakEnabled;

        public Dictionary<ConfigDefinition, ConfigEntryBase> tweakConfigs;

        /// <summary>
        /// Unique ID for the patch.
        /// </summary>
        /// <value>The identifier.</value>
        public string ID { get; protected set; }
        /// <summary>
        /// Human readable name for the patch.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; protected set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:LLBTweaker.TweakBase"/> is enabled.
        /// </summary>
        /// <value><c>true</c> if the patch is curently enabled, <c>false</c> otherwise.</value>
        public bool IsEnabled { get; protected set; } = false;

        /// <summary>
        /// Initializes a new instance of <see cref="T:LLBTweaker.TweakBase"/>.
        /// </summary>
        /// <param name="tweakID">Patch's unique identifier.</param>
        /// <param name="tweakName">Patch's human readable name.</param>
        protected TweakBase(string tweakID, string tweakName)
        {
            this.ID = tweakID;
            this.Name = tweakName;
            this.tweakEnabled = Config.Bind("TweakToggles", tweakID, true);
            this.tweakConfigs = new Dictionary<ConfigDefinition, ConfigEntryBase>();
            Logger.LogInfo(this.ID + " settedUp");
        }

        /// <summary>
        /// Method that will be called by LLBTweaker to apply the patch.
        /// </summary>
        protected abstract void DoPatch();
        /// <summary>
        /// Method that will be called by LLBTweaker to remove the patch.
        /// </summary>
        protected abstract void DoUnpatch();

        /// <summary>
        /// Method to check if the patch should be applied. Will be called by LLBTweaker during state updates.
        /// </summary>
        /// <returns><c>true</c> if the patch should be applied, <c>false</c> otherwise.</returns>
        public bool ShouldPatch()
        {
            return !IsEnabled && tweakEnabled.Value;
        }
        /// <summary>
        /// Method to check if the patch should be removed. Will be called by LLBTweaker during state updates.
        /// </summary>
        /// <returns><c>true</c> if the patch should be removed, <c>false</c> otherwise.</returns>
        public bool ShouldUnpatch()
        {
            return IsEnabled && !tweakEnabled.Value;
        }

        /// <summary>
        /// Update the tweak state to correspond to current configs.
        /// </summary>
        public void UpdateState()
        {
            if (this.ShouldPatch()) this.DoPatch();
            if (this.ShouldUnpatch()) this.DoUnpatch();
        }

        /// <summary>
        /// Enables the tweak.
        /// </summary>
        public void Enable()
        {
            this.tweakEnabled.Value = true;
            this.UpdateState();
        }
        /// <summary>
        /// Disables the tweak.
        /// </summary>
        public void Disable()
        {
            this.tweakEnabled.Value = false;
            this.UpdateState();
        }
        /// <summary>
        /// Toggles the tweak's state.
        /// </summary>
        public void Toggle()
        {
            this.tweakEnabled.Value = !this.tweakEnabled.Value;
            this.UpdateState();
        }

        /// <summary>
        /// Reapplies a tweak. Useful to update changed configs.
        /// </summary>
        public void Reload()
        {
            this.DoUnpatch();
            this.UpdateState();
        }

        public void AddConfig(ConfigEntryBase config)
        {
            this.tweakConfigs.Add(config.Definition, config);
        }

        public TweakConfigJson ConfigToJson()
        {
            TweakConfigJson jTweakObject = new TweakConfigJson
            {
                enabled = tweakEnabled.Value
            };
            List<ConfigEntryJson> jTweakConfigs = new List<ConfigEntryJson>();
            foreach (var config in tweakConfigs.Values)
            {
                jTweakConfigs.Add(new ConfigEntryJson() {
                    Key = config.Definition.Key,
                    Section = config.Definition.Section,
                    value = config.GetSerializedValue(),
                });
            }
            jTweakObject.configs = jTweakConfigs;
            return jTweakObject;
        }

        public void LoadJsonConfig(TweakConfigJson jTweakObject)
        {
            this.tweakEnabled.Value = jTweakObject.enabled;

            foreach (ConfigEntryJson jTweakConfig in jTweakObject.configs)
            {
                ConfigDefinition def = new ConfigDefinition(jTweakConfig.Section, jTweakConfig.Key);
                Config[def].SetSerializedValue(jTweakConfig.value);
            }
        }

        #region UnityScriptMethods
        public virtual void Start() {}
        public virtual void Update() {}
        public virtual void FixedUpdate() {}
        public virtual void LateUpdate() {}
        public virtual void OnGUI() {}
        #endregion
    }

}
