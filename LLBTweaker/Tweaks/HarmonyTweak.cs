﻿using System;
using System.Collections.Generic;
using BepInEx.Configuration;
using BepInEx.Logging;
using HarmonyLib;

namespace LLBT.Tweaks
{
    /// <summary>
    /// A class extending <see cref="TweakBase"/> to interface harmony patches
    /// </summary>
    public class HarmonyTweak : TweakBase
    {
        /// <summary>
        /// The type of the class that holds the harmony patches for this tweak.
        /// </summary>
        protected List<Type> patchClasses;
        /// <summary>
        /// The harmony instance that will hold the tweak's modifications.
        /// </summary>
        private readonly Harmony harmonyInstance;

        /// <summary>
        /// Initializes a new instance of <see cref="HarmonyTweak"/> that will manage the specified patch class.
        /// </summary>
        /// <returns>The instanciated <see cref="HarmonyTweak"/> with default parameters.</returns>
        /// <param name="patchClass"> The type of the class holding harmony patches.</param>
        public static HarmonyTweak Create(Type patchClass)
        {
            string tweakID = "HarmonyTweak_" + patchClass.Name;
            string tweakName = patchClass.Name;
            HarmonyTweak tweak = new HarmonyTweak(tweakID, tweakName);
            tweak.AddPatchClass(patchClass);
            return tweak;
        }

        /// <summary>
        /// Initializes a new instance of <see cref="HarmonyTweak"/> that will manage the specified patch class.
        /// </summary>
        /// <param name="tweakID">Patch identifier.</param>
        /// <param name="tweakName">Patch name.</param>
        /// <param name="harmonyInstance">Harmony instance.</param>
        public HarmonyTweak(string tweakID, string tweakName, Harmony harmonyInstance = null)
            : base(tweakID, tweakName)
        {
            this.harmonyInstance = harmonyInstance ?? new Harmony(tweakID);
            this.patchClasses = new List<Type>();
        }

        protected void AddPatchClass(Type patchClass, bool addConfig = false)
        {
            this.patchClasses.Add(patchClass);
            if (addConfig)
            {
                ConfigEntry<bool> patchToggle = Config.Bind<bool>("PatchToggles."+this.ID, patchClass.Name, true);
                this.AddConfig(patchToggle);
            }
        }

        #region overrides
        /// <inheritdoc cref="TweakBase.DoPatch()"/>
        protected override void DoPatch()
        {
            if (!this.IsEnabled)
            {
                Logger.LogDebug("Applying " + this.Name);
                foreach (Type patchClass in patchClasses) { 
                    this.harmonyInstance.PatchAll(patchClass);
                }
                this.IsEnabled = true;
            }
        }

        /// <inheritdoc cref="TweakBase.DoUnpatch()"/>
        protected override void DoUnpatch()
        {
            if (this.IsEnabled)
            {
                Logger.LogDebug("Unpatching " + this.Name);
                this.harmonyInstance.UnpatchSelf();
                this.IsEnabled = false;
            }
        }
        #endregion
    }
}
