﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using BepInEx;
using BepInEx.Configuration;
using BepInEx.Logging;
using LLBML.Networking;
using LLBML.Players;
using LLBML.States;
using LLBML.Utils;
using LLBT.Tweaks;
using Multiplayer;

namespace LLBT
{
    [BepInPlugin(PluginInfos.PLUGIN_ID, PluginInfos.PLUGIN_NAME, PluginInfos.PLUGIN_VERSION)]
    [BepInDependency(LLBML.PluginInfos.PLUGIN_ID, BepInDependency.DependencyFlags.HardDependency)]
    [BepInDependency("no.mrgentle.plugins.llb.modmenu", BepInDependency.DependencyFlags.SoftDependency)]
    public class LLBTweaker : BaseUnityPlugin
    {
        private int currentTweakIndex = 0;
        internal ConfigEntry<KeyCode> cycleTweaksNextKey;
        internal ConfigEntry<KeyCode> cycleTweaksPreviousKey;
        internal ConfigEntry<bool> enableLiveTweaking;
        internal ConfigEntry<bool> restoreConfigsOnEnteringLobby;
        internal ConfigEntry<KeyCode> reloadTweaksStateKey;

        internal ConfigEntry<KeyCode> toggleCurrentTweakKey;


        internal List<TweakBase> tweaks = new List<TweakBase>();
        internal static ManualLogSource Log { get; private set; } = null;
        internal static ManualLogSource TweakLog { get; private set; } = null;
        internal static LLBTweaker Instance  { get; private set; } = null;
        internal ConfigFile TweakConfig { get; private set; } = null;

        private void Awake()
        {


            Log = this.Logger;
            Instance = this;
            Logger.LogInfo("Hello, world!");
            InitConfigs();

            TweakLog = BepInEx.Logging.Logger.CreateLogSource("Tweaks");
            this.TweakConfig = new ConfigFile(
                Path.Combine(BepInEx.Paths.ConfigPath, "TweakConfigs.cfg"),
                true,
                this.Info.Metadata
            );
        }

        private void Start()
        {
            this.TweakConfig.SaveOnConfigSet = false;

            Network.Init();
            ModDependenciesUtils.RegisterToModMenu(this.Info);

            ForAllTweaks(tweak => tweak.Start(), "Start");
            foreach (TweakBase tweak in tweaks.Where((TweakBase tweak) => tweak.ShouldPatch()))
            {
                Logger.LogDebug("Applying tweak " + tweak.ID);
                tweak.Enable();
            }
        }

        private void Update()
        {
            if (GameStates.IsInOnlineLobby()) Network.OnUpdate();
            else showVanillaWarning = false;
            if (enableLiveTweaking.Value && tweaks.Count > 0)
            {
                HandleLiveTweaking();
            }
            ForAllTweaks(tweak => tweak.Update(), "Update");
        }

        internal bool showVanillaWarning;
        private void OnGUI()
        {

            if (showVanillaWarning)
            {

            }

            if (!enableLiveTweaking.Value || tweaks.Count == 0) return;

            GUIStyle bold = new GUIStyle
            {
                fontStyle = FontStyle.Bold,
                fontSize = 24,
                normal =
                {
                    textColor = Color.red
                }
            };

            TweakBase currentTweak = tweaks[currentTweakIndex];
            GUI.Label(new Rect(20, 20, 400, 25), "Current Tweak: " + currentTweak.Name, bold);
            //GUI.Label(new Rect(420, 20, 200, 25), "Toggle with: " + toggleCurrentPatchKey.Value.ToString(), bold);
            GUI.Label(new Rect(20, 50, 200, 25), "Is Tweak Enabled: " + currentTweak.IsEnabled, bold);

            ForAllTweaks(tweak => tweak.OnGUI(), "OnGUI");
        }

        private void HandleLiveTweaking()
        {
            if (Input.GetKeyDown(toggleCurrentTweakKey.Value))
            {
                if (NetworkApi.IsOnline)
                {
                    var localPlayer = Player.GetLocalPlayer();
                    if (localPlayer.nr == 0 && GameStates.IsInLobby())
                    {
                        tweaks[currentTweakIndex].Toggle();
                        GameStatesLobbyUtils.SendPlayerState(localPlayer);
                    }
                }
                else
                {
                    tweaks[currentTweakIndex].Toggle();
                }
            }
            if (Input.GetKeyDown(cycleTweaksNextKey.Value))
            {
                currentTweakIndex--;
                if (currentTweakIndex < 0) currentTweakIndex = tweaks.Count - 1;
            }
            if (Input.GetKeyDown(cycleTweaksPreviousKey.Value))
            {
                currentTweakIndex++;
                if (currentTweakIndex >= tweaks.Count) currentTweakIndex = 0;
            }
            if (Input.GetKeyDown(reloadTweaksStateKey.Value))
            {
                RestoreAllTweaksConfigAndStates();
            }
        }


        public static void AddTweak(TweakBase tweak)
        {
            Instance.tweaks.Add(tweak);
        }

        internal void DisableAll()
        {
            Logger.LogDebug("Disabling all tweaks.");
            ForAllTweaks(tweak => tweak.Disable(), "Disable");
        }
        internal void LoadTweakConfigs(Dictionary<string, TweakConfigJson> jTweakConfigs)
        {
            foreach (var jTweakConfig in jTweakConfigs)
            {
                Logger.LogDebug($"{jTweakConfig.Key}:\n{jTweakConfig.Value}");
                if (LLBTweaker.Instance.tweaks.Find(_tweak => _tweak.ID == jTweakConfig.Key) is TweakBase tweak)
                {
                    tweak.LoadJsonConfig(jTweakConfig.Value);
                }
                else
                {
                    throw new KeyNotFoundException("Found config for tweak: " + jTweakConfig.Key + ", which is unknown!");
                }
            }
        }


        internal void ReloadAllTweaksConfigs()
        {
            Logger.LogDebug("Reloading all tweaks configs.");
            TweakConfig.Reload();
        }

        internal void UpdateAllTweaksStates()
        {
            Logger.LogDebug("Reloading all tweaks states.");
            ForAllTweaks(tweak => tweak.Update(), "Update");
        }

        internal void ReloadAllTweaksStates()
        {
            Logger.LogDebug("Reloading all tweaks states.");
            ForAllTweaks(tweak => tweak.Reload(), "Reload");
        }

        internal void RestoreAllTweaksConfigAndStates()
        {
            ReloadAllTweaksConfigs();
            ReloadAllTweaksStates();
        }


        private void InitConfigs()
        {
            toggleCurrentTweakKey = Config.Bind("Keybinds", "toggleCurrentTweak", KeyCode.Backslash);
            cycleTweaksNextKey = Config.Bind("Keybinds", "cycleTweaksNext", KeyCode.LeftBracket);
            cycleTweaksPreviousKey = Config.Bind("Keybinds", "cycleTweaksPrevious", KeyCode.RightBracket);
            reloadTweaksStateKey = Config.Bind("Keybinds", "reloadTweaksState", KeyCode.Backspace);

            Config.Bind("General", "headerGeneral", "General:", new ConfigDescription("", null, "modmenu_header"));
            enableLiveTweaking = Config.Bind("General", "enableLiveTweaking", false);
            restoreConfigsOnEnteringLobby = Config.Bind("General", "restoreConfigsOnEnteringLobby", true);
        }

        private void ForAllTweaks(Action<TweakBase> callback, string desc = null)
        {
            foreach (var tweak in tweaks)
            {
                try
                {
                    callback(tweak);
                }
                catch (Exception e)
                {
                    Logger.LogError($"Caught exception while trying to call {desc ?? "something (see stacktrace)"} on {tweak.Name}:");
                    Logger.LogError(e);
                }
            }
        }


        #region UnityScriptMethods
        private void FixedUpdate()
        {
            ForAllTweaks(tweak => tweak.FixedUpdate(), "FixedUpdate");
        }
        private void LateUpdate()
        {
            ForAllTweaks(tweak => tweak.LateUpdate(), "LateUpdate");
        }
        #endregion

    }
}
