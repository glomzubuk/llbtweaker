﻿using System;
using System.Runtime.CompilerServices;

using GameplayEntities;
using LLBT;
using HarmonyLib;
using LLBT.Tweaks;

namespace SampleTweaks
{
    public class SampleHarmonyTweak : HarmonyTweak
    {
        public SampleHarmonyTweak(): base("doublejump-tweak", "No Double Jumps")
        {
            this.patchClasses.Add(typeof(NoDoubleJumps));
            this.patchClasses.Add(typeof(NoDoubleJumps2));
            this.patchClasses.Add(typeof(NoDoubleJumps3));
        }
    }

    public static class NoDoubleJumps
    {
        [HarmonyPatch(typeof(MovableEntity), nameof(MovableEntity.HasExtraJumpLeft)), HarmonyPostfix]
        public static void HasExtraJumpLeft_Postfix(ref bool __result, MovableEntity __instance)
        {
            __result = __instance.moveableData.extraJumps > 1;
        }
    }

    public static class NoDoubleJumps2
    {
        [HarmonyPatch(typeof(MovableEntity), nameof(MovableEntity.HasExtraJumpLeft)), HarmonyPostfix]
        public static void HasExtraJumpLeft_Postfix(ref bool __result, MovableEntity __instance)
        {
            HarmonyTweak.Logger.LogDebug("Jumps left:" + __instance.moveableData.extraJumps);
            __result = __instance.moveableData.extraJumps > 1;
        }
    }
    public static class NoDoubleJumps3
    {
        [HarmonyPatch(typeof(MovableEntity), nameof(MovableEntity.HasExtraJumpLeft)), HarmonyPostfix]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static void HasExtraJumpLeft_Postfix(ref bool __result, MovableEntity __instance)
        {
            var garbage_variable = "This doesn't work if there's only a few instructions";
            garbage_variable += "how weird";
            __result = __instance.moveableData.extraJumps > 1;
        }
    }
}
