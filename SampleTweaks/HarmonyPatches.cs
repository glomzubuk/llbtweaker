﻿using HarmonyLib;
using GameplayEntities;
using LLBML.Math;

namespace SampleTweaks
{
    public static class DisableBallSpeedDecay
    {
        [HarmonyPatch(typeof(HittingEntity), nameof(HittingEntity.SetFlySpeedMultipliedToEntity)), HarmonyPrefix]
        public static bool SetFlySpeedMultipliedToEntity_Prefix(HitableEntity hitEntity, ref HHBCPNCDNDH __result)
        {
            hitEntity.SetFlySpeedMultiplied((Floatf)2);
            __result = hitEntity.hitableData.flySpeed;
            return false;
        }
    }

    public static class NoLandingLag
    {
        [HarmonyPatch(typeof(PlayerEntity), nameof(PlayerEntity.SetEntityValues)), HarmonyPostfix]
        public static void NoLandingLag_Postfix(PlayerEntity __instance)
        {

            Traverse.Create(__instance).Field<HHBCPNCDNDH>("landDuration").Value = __instance.framesDuration60fps(1);
            Traverse.Create(__instance).Field<HHBCPNCDNDH>("startRunDuration").Value = __instance.framesDuration60fps(1);
        }
    }

    public static class NoTurnRunLag
    {
        [HarmonyPatch(typeof(PlayerEntity), nameof(PlayerEntity.SetEntityValues)), HarmonyPostfix]
        public static void NoTurnRunLag_Postfix(PlayerEntity __instance)
        {
            Traverse.Create(__instance).Field<int>("turnRunFrames").Value = 1;
        }
    }

    public static class DisableBall
    {
        [HarmonyPatch(typeof(BallEntity), nameof(BallEntity.Spawn)), HarmonyPrefix]
        public static bool Spawn_Prefix(BallEntity __instance, int toPlayer)
        {
            __instance.CleanUp();
            __instance.ballData.lastHitterIndex = toPlayer;
            __instance.SetBallState(BallState.NOT_INGAME, HitstunState.NONE);
            return false;
        }
    }
}