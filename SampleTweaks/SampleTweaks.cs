﻿using BepInEx;
using LLBT;
using LLBT.Tweaks;

namespace SampleTweaks
{
    [BepInPlugin("fr.glomzubuk.plugins.llb.sampletweakcollection", "SampleTweaks" , "0.0.1")]
    [BepInDependency(LLBT.PluginInfos.PLUGIN_ID, BepInDependency.DependencyFlags.HardDependency)]
    public class SampleTweaks : BaseUnityPlugin
    {

        public void Awake()
        {
            LLBTweaker.AddTweak(new SampleHarmonyTweak());
            LLBTweaker.AddTweak(HarmonyTweak.Create(typeof(DisableBallSpeedDecay)));
            LLBTweaker.AddTweak(HarmonyTweak.Create(typeof(DisableBall)));
            LLBTweaker.AddTweak(HarmonyTweak.Create(typeof(NoLandingLag)));
            LLBTweaker.AddTweak(HarmonyTweak.Create(typeof(NoTurnRunLag)));
        }
    }
}
